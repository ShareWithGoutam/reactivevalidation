import { Component } from '@angular/core';
import{FormGroup, FormControl, Validators} from '@angular/forms'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  form=new FormGroup({
   firstName : new FormControl('',Validators.required),
   lastname : new FormControl('',Validators.required),
   Email : new FormControl('',[Validators.required,Validators.email]),
   password : new FormControl('',[Validators.required,Validators.minLength(8)]),
   gender:new FormControl(),
   ucountry:new FormControl()

  });

  constructor() { }
  get firstname(){
    return this.form.get("firstName")
  }
  ngOnInit() {

  }
  submit(){
    debugger;
    console.log(this.form.value)
  }

}
